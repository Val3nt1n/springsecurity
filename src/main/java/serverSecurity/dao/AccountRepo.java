package serverSecurity.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepo extends JpaRepository<Account, Integer> {
    Account findByUsername(String username);
}