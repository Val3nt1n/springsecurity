package serverSecurity.dao;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

public class AccountController {

    AccountRepo repo;

    @RequestMapping("/accounts")
    @ResponseBody
    public List<Account> getAccounts(){

        return repo.findAll();
    }

}
